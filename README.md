#Docker-Nginx-Slim Example

An example slim application using PHP 7.1.5 running on docker.

The base image used for the Docker image is linux alpine, a minimum
image builds an image of 23.4MB for the entire slim application.

Usage
===

1. Build the docker image:
```
docker build -t php-nginx-slim:latest .
```

or build in dev mode (which also installs xdebug and updates the ENV variable in nginx):
```
docker build --build-arg=ENVIRONMENT=dev -t php-nginx-slim:latest .
```

2. Run the image using docker-compose:
```
docker-compose up -d
```

or manually using docker run:
```
docker run -d -v .:/data -p 80:80 php-nginx-slim:latest
```

3. To to `http://localhost` and it should display system information.

Debugging
===

You can inspect the nginx access/error logs using:
```
docker logs <NAME_OF_CONTAINER>
```