FROM alpine:edge

MAINTAINER Jack Timblin <jacktimblin@gmail.com>

#set the build environment.
ARG ENVIRONMENT=prod

#Add the testing repository.
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

#Install the main php packages.
RUN set -xe && \
    apk update && apk add curl php7-fpm php7-json php7-mbstring

#Copy the repository to the container.
COPY . /data

# Install and run composer.
RUN set -xe && \
    apk add --no-cache --virtual .build-deps git curl php7-phar php7-openssl php7-zlib && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/bin/composer && chmod -R 777 /usr/bin/composer && \
    cd /data && composer install && \
    apk del .build-deps && rm /usr/bin/composer

# Install xdebug if we are on a development build
RUN set -xe && \
    if [[ $ENVIRONMENT = 'dev' ]]; then \
        apk add --no-cache --virtual .build-deps make gcc g++ autoconf openssl php7-dev && \
        wget http://xdebug.org/files/xdebug-2.5.3.tgz && tar -xvzf xdebug-2.5.3.tgz && \
        cd xdebug-2.5.3 && phpize && ./configure && make && \
        cp modules/xdebug.so /usr/lib/php7/modules && \
        apk del .build-deps && cd ../ && rm xdebug-2.5.3.tgz && rm -rf xdebug-2.5.3 && \
        cp /data/docker/xdebug.ini /etc/php7/conf.d/xdebug.ini; \
    fi

#install nginx.
RUN set -xe && \
    apk add nginx bash && \
    mkdir /run/nginx

#copy our nginx file across to the correct directory.
RUN if [[ -f /etc/nginx/conf.d/default.conf ]]; then rm /etc/nginx/conf.d/default.conf; fi && \
 cp /data/docker/nginx.conf /etc/nginx/conf.d/default.conf

#push our build environment into the nginx config.
RUN sed -i "s/:environment:/$ENVIRONMENT/g" /etc/nginx/conf.d/default.conf

#symlink the access and error logs to stdout and stderr so we can view them using docker logs.
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

#expose our port.
EXPOSE 80

#tell the container what the stop signal is.
STOPSIGNAL SIGQUIT

#the default command to be ran if one is not supplied.
CMD ["/bin/bash", "/data/docker/docker-entrypoint"]
