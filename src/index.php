<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\App as Application;

define('ENVIRONMENT', $_SERVER['ENV'] ?: 'dev');

require '../vendor/autoload.php';

$app = new Application();

//Just outputs application information.
$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write(
        'Slim Version: ' . Application::VERSION . '</br>' .
        'PHP Version: ' . phpversion() . '</br>' .
        'OS Version: ' . php_uname() . '</br>'
    );

    return $response;
});

$app->run();